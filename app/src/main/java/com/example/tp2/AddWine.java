package com.example.tp2;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.util.Log;

public class AddWine extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine1);

        Button save = (Button) findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                /**
                 * On récupère les données écrites dans les textview
                 */
                EditText editName = (EditText) findViewById(R.id.wineName);
                EditText editRegion = (EditText) findViewById(R.id.editWineRegion);
                EditText editLoc = (EditText) findViewById(R.id.editLoc);
                EditText editClimate = (EditText) findViewById(R.id.editClimate);
                EditText editPA = (EditText) findViewById(R.id.editPlantedArea);

                /**
                 * On caste les données récupérées en String (de base)
                 */
                String name = String.valueOf(editName.getText());
                String region = String.valueOf(editRegion.getText());
                String loc = String.valueOf(editLoc.getText());
                String climate = String.valueOf(editClimate.getText());
                String plantedArea = String.valueOf(editPA.getText());

                Log.d(" ajout ", "name " + name);
                Log.d(" ajout ", "region " + region);

                /**
                 * On vérifie si tous les champs sont bien remplis, si oui on rentre dans la condition
                 */
                if (!name.isEmpty() && !region.isEmpty())
                {
                    Wine wine = new Wine(name, region, loc, climate, plantedArea);
                    WineDbHelper wineDbHelper = new WineDbHelper(AddWine.this);
                    wineDbHelper.addWine(wine);

                    Toast.makeText(AddWine.this, "Sauvegarde effectuée", Toast.LENGTH_LONG).show();

                    Intent intent1 = new Intent(AddWine.this, MainActivity.class);
                    startActivity(intent1);
                    finish();
                }
                /**
                 * Si le champ n'est pas rempli, on affiche un message d'alerte
                 */
                else
                {
                    Toast.makeText(AddWine.this, "Le titre n'est pas renseigné", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}