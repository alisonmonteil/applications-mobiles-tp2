package com.example.tp2;

import android.content.Intent;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class MainActivity extends AppCompatActivity
{
    private WineDbHelper wineDbHelper;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**
         * On commence par set la listview et créer un curseur avec les données
         */
        wineDbHelper = new WineDbHelper(this);
        final Cursor wine = wineDbHelper.getReadableDatabase().query(
                WineDbHelper.TABLE_NAME,
                new String[] {
                        "ROWID AS " + WineDbHelper._ID,
                        WineDbHelper.COLUMN_NAME,
                        WineDbHelper.COLUMN_WINE_REGION,
                        WineDbHelper.COLUMN_LOC,
                        WineDbHelper.COLUMN_CLIMATE,
                        WineDbHelper.COLUMN_PLANTED_AREA},
                null,
                null,
                null,
                null,
                WineDbHelper.COLUMN_NAME);

        Cursor cursor = wineDbHelper.fetchAllWines();

        /**
         * On crée un SimpleCursorAdapter qui permettra de lier le curseur à la vue d'adaptateur à l'aide
         * de la présentation de la vue d'élement de liste (avec 2 éléments)
         */
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.wine,
                cursor,
                new String[]{
                        WineDbHelper.COLUMN_NAME,
                        WineDbHelper.COLUMN_WINE_REGION},
                new int[]{
                        R.id.name,
                        R.id.region},
                0);

        /**
         * On associe l'adaptateur avec la listview
         */
        listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);

        /**
         * Permet de supprimer un vin lorsque l'on effectue une pression prolongée sur celui-ci
         */
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                final Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                alertDialog.setNeutralButton("Supprimer", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        WineDbHelper winedbhelper = new WineDbHelper(MainActivity.this);
                        winedbhelper.deleteWine(cursor);

                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                        Toast.makeText(MainActivity.this, "Suppression réussie", Toast.LENGTH_LONG).show();
                    }
                });
                alertDialog.show();
                return true;
            }
        });

        /**
         * Permet d'enregistrer un rappel à appeler lorsqu'un élément de ce AdapterView a été cliqué
         * (écouteur d'évènement)
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Cursor wine = (Cursor) adapter.getItem(position);
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", WineDbHelper.cursorToWine(wine));
                startActivity(intent);
            }
        });

        /**
         * Permet d'ajouter un vin à la liste
         */
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MainActivity.this, AddWine.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Barre de menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        /**
         * Inflate the menu; this adds items to the action bar if it is present.
         */
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        /**
         * Handle action bar item clicks here. The action bar will
         * automatically handle clicks on the Home/Up button, so long
         * as you specify a parent activity in AndroidManifest.xml.
         */
        int id = item.getItemId();

        /**
         * noinspection SimplifiableIfStatement
         */
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}