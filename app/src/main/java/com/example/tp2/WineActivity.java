package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class WineActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        final Wine wine = intent.getParcelableExtra("wine");

        final EditText wineName = findViewById(R.id.wineName);
        wineName.setText(wine.getTitle());

        final EditText editWineRegion = findViewById(R.id.editWineRegion);
        editWineRegion.setText(wine.getRegion());

        final EditText editLoc = findViewById(R.id.editLoc);
        editLoc.setText(wine.getLocalization());

        final EditText editClimate = findViewById(R.id.editClimate);
        editClimate.setText(wine.getClimate());

        final EditText editPlantedArea = findViewById(R.id.editPlantedArea);
        editPlantedArea.setText(wine.getPlantedArea());

        /**
         * Permet de faire le lien de connexion entre l'auditeur et le bouton
         * Le système exécute le code après que l'utilisateur ait appuyé sur le bouton
         */
        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                /**
                 * On récupère les données écrites dans les textview
                 */
                EditText editName = (EditText) findViewById(R.id.wineName);
                EditText editRegion = (EditText) findViewById(R.id.editWineRegion);
                EditText editLoc = (EditText) findViewById(R.id.editLoc);
                EditText editClimate = (EditText) findViewById(R.id.editClimate);
                EditText editPA = (EditText) findViewById(R.id.editPlantedArea);

                /**
                 * On caste les données récupérées en String (de base)
                 */
                String name = editName.getText().toString();
                String region = editRegion.getText().toString();
                String loc = editLoc.getText().toString();
                String climate = editClimate.getText().toString();
                String plantedArea = editPA.getText().toString();

                /**
                 * On vérifie si tous les champs sont bien remplis, si oui on rentre dans la condition
                 */
                if (!name.isEmpty() && !region.isEmpty() && !loc.isEmpty() && !climate.isEmpty() && !plantedArea.isEmpty())
                {
                    Wine wine = new Wine(name, region, loc, climate, plantedArea);
                    WineDbHelper wineDbHelper = new WineDbHelper(WineActivity.this);
                    wineDbHelper.updateWine(wine);

                    Toast.makeText(WineActivity.this, "Sauvegarde réussie", Toast.LENGTH_LONG).show();

                    Intent intent1 = new Intent(WineActivity.this, MainActivity.class);
                    startActivity(intent1);
                    finish();
                }
                /**
                 * Si le champ n'est pas rempli, on affiche un message d'alerte
                 */
                else {
                    Toast.makeText(WineActivity.this, "Un champ est vide", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}