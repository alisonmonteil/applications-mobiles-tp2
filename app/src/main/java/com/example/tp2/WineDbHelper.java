package com.example.tp2;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class WineDbHelper extends SQLiteOpenHelper
{
    private static final String TAG = WineDbHelper.class.getSimpleName();

    /**
     * If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 15;
    public static final String DATABASE_NAME = "wine.db";
    public static final String TABLE_NAME = "cellar";
    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE cellar ("
                + _ID + " NUMERIC, "
                + COLUMN_NAME + " TEXT UNIQUE , "
                + COLUMN_WINE_REGION + " TEXT, "
                + COLUMN_LOC + " TEXT, "
                + COLUMN_CLIMATE + " TEXT, "
                + COLUMN_PLANTED_AREA + " TEXT, UNIQUE ("
                + COLUMN_NAME + ", "
                + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK);");

        populate();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        long newId = DatabaseUtils.longForQuery(
                db,
                "SELECT COUNT(*) FROM " + TABLE_NAME,
                null) + 1;

        /**
         * Inserting Row
         */
        long rowID;
        ContentValues contentValues = new ContentValues();

        contentValues.put(_ID, newId);
        contentValues.put(COLUMN_NAME, wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION, wine.getRegion());
        contentValues.put(COLUMN_LOC, wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE, wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        if (wine.getTitle() == null)
            wine.setTitle("");

        if (wine.getRegion() == null)
            wine.setRegion("");

        if (wine.getLocalization() == null)
            wine.setLocalization("");

        if (wine.getClimate() == null)
            wine.setClimate("");

        if (wine.getPlantedArea() == null)
            wine.setPlantedArea("");

        rowID = db.insert(TABLE_NAME, _ID, contentValues);
        Log.d(TAG, "addWine : rowID = " + rowID);
        Log.d(TAG, "addWine : newID = " + newId);

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_NAME, wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION, wine.getRegion());
        contentValues.put(COLUMN_LOC, wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE, wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        int res = db.update(TABLE_NAME, contentValues, _ID + " = ?", new String[]
                {Long.toString(wine.getId())});

        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);

        if (cursor != null)
        {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteWine(Cursor cursor)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?", new String[]
                {Long.toString(cursor.getLong(0))});
    }

    public void populate()
    {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
    }

    public static Wine cursorToWine(Cursor cursor)
    {
        return new Wine(cursor.getLong(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(5),
                cursor.getString(4));
    }
}